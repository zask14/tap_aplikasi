<!DOCTYPE html>
<html lang="en">
<head>
	<title>testing yudiana</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
	<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
	<script type="text/javascript" src="id-jk-jakbar.js"></script>
</head>
<body>
	<h1>2. buatkan satu halaman untuk menampilkan peta, dan tampilkan file geojson(terlampir) diatas layer peta tersebut</h1>
	<a href="/"><h1>Back</h1></a>
  	<div id="map" style="width: 100%; height: 700px;"></div>
  	<script type="text/javascript">
  		const map = L.map('map').setView([-6.13712, 106.70857], 12);

		const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 19,
			attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
		}).addTo(map);

		L.geoJSON(testing).addTo(map);

  	</script>

</body>
</html>