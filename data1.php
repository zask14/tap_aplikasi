<?php
//******************************************************************** DATA range Pria ***********************************************//
$range04 = array(
	array("label"=> "2019", "y"=> 453157),
	array("label"=> "2020", "y"=> 341196),
	array("label"=> "2021", "y"=> 409299)
);

$range59 = array(
	array("label"=> "2019", "y"=> 478490),
	array("label"=> "2020", "y"=> 461099),
	array("label"=> "2021", "y"=> 409708)
);

$range1014 = array(
	array("label"=> "2019", "y"=> 404210),
	array("label"=> "2020", "y"=> 450271),
	array("label"=> "2021", "y"=> 416558)
);

$range1519 = array(
	array("label"=> "2019", "y"=> 362525),
	array("label"=> "2020", "y"=> 424040),
	array("label"=> "2021", "y"=> 425852)
);

$range2024 = array(
	array("label"=> "2019", "y"=> 362699),
	array("label"=> "2020", "y"=> 417744),
	array("label"=> "2021", "y"=> 432874)
);

$range2529 = array(
	array("label"=> "2019", "y"=> 445352),
	array("label"=> "2020", "y"=> 418669),
	array("label"=> "2021", "y"=> 435503)
);

$range3034 = array(
	array("label"=> "2019", "y"=> 515860),
	array("label"=> "2020", "y"=> 434102),
	array("label"=> "2021", "y"=> 445591)
);

$range3539 = array(
	array("label"=> "2019", "y"=> 501470),
	array("label"=> "2020", "y"=> 490160),
	array("label"=> "2021", "y"=> 440327)
);

$range4044 = array(
	array("label"=> "2019", "y"=> 439041),
	array("label"=> "2020", "y"=> 458286),
	array("label"=> "2021", "y"=> 431464)
);

$range4549 = array(
	array("label"=> "2019", "y"=> 371257),
	array("label"=> "2020", "y"=> 406222),
	array("label"=> "2021", "y"=> 396281)
);

$range5054 = array(
	array("label"=> "2019", "y"=> 307256),
	array("label"=> "2020", "y"=> 344192),
	array("label"=> "2021", "y"=> 342251)
);

$range5559 = array(
	array("label"=> "2019", "y"=> 239492),
	array("label"=> "2020", "y"=> 261019),
	array("label"=> "2021", "y"=> 280483)
);

$range6064 = array(
	array("label"=> "2019", "y"=> 175601),
	array("label"=> "2020", "y"=> 182138),
	array("label"=> "2021", "y"=> 208315)
);

$range65 = array(
	array("label"=> "2019", "y"=> 228911),
	array("label"=> "2020", "y"=> 245643),
	array("label"=> "2021", "y"=> 288242)
);

//******************************************************************** DATA range wanita ***********************************************//
$range04w = array(
	array("label"=> "2019", "y"=> 435687),
	array("label"=> "2020", "y"=> 319822),
	array("label"=> "2021", "y"=> 389749)
);

$range59w = array(
	array("label"=> "2019", "y"=> 462403),
	array("label"=> "2020", "y"=> 432664),
	array("label"=> "2021", "y"=> 391131)
);

$range1014w = array(
	array("label"=> "2019", "y"=> 380844),
	array("label"=> "2020", "y"=> 424594),
	array("label"=> "2021", "y"=> 396706)
);

$range1519w = array(
	array("label"=> "2019", "y"=> 357569),
	array("label"=> "2020", "y"=> 401788),
	array("label"=> "2021", "y"=> 408996)
);

$range2024w = array(
	array("label"=> "2019", "y"=> 393490),
	array("label"=> "2020", "y"=> 400325),
	array("label"=> "2021", "y"=> 421508)
);

$range2529w = array(
	array("label"=> "2019", "y"=> 483771),
	array("label"=> "2020", "y"=> 416968),
	array("label"=> "2021", "y"=> 423712)
);

$range3034w = array(
	array("label"=> "2019", "y"=> 507359),
	array("label"=> "2020", "y"=> 427007),
	array("label"=> "2021", "y"=> 437712)
);

$range3539w = array(
	array("label"=> "2019", "y"=> 482121),
	array("label"=> "2020", "y"=> 484197),
	array("label"=> "2021", "y"=> 433312)
);

$range4044w = array(
	array("label"=> "2019", "y"=> 422799),
	array("label"=> "2020", "y"=> 455099),
	array("label"=> "2021", "y"=> 425462)
);

$range4549w = array(
	array("label"=> "2019", "y"=> 359079),
	array("label"=> "2020", "y"=> 393122),
	array("label"=> "2021", "y"=> 391664)
);

$range5054w = array(
	array("label"=> "2019", "y"=> 302979),
	array("label"=> "2020", "y"=> 326379),
	array("label"=> "2021", "y"=> 338140)
);

$range5559w = array(
	array("label"=> "2019", "y"=> 246068),
	array("label"=> "2020", "y"=> 265384),
	array("label"=> "2021", "y"=> 280101)
);

$range6064w = array(
	array("label"=> "2019", "y"=> 183612),
	array("label"=> "2020", "y"=> 195498),
	array("label"=> "2021", "y"=> 213962)
);

$range65w = array(
	array("label"=> "2019", "y"=> 254708),
	array("label"=> "2020", "y"=> 284460),
	array("label"=> "2021", "y"=> 329873)
);

//*************************************************************** DATA Jumlah PRIA DAN WANITA ***********************************************//
$range04ttl = array(
	array("label"=> "2019", "y"=> 888844),
	array("label"=> "2020", "y"=> 661018),
	array("label"=> "2021", "y"=> 799048)
);

$range59ttl = array(
	array("label"=> "2019", "y"=> 940893),
	array("label"=> "2020", "y"=> 893762),
	array("label"=> "2021", "y"=> 800839)
);

$range1014ttl = array(
	array("label"=> "2019", "y"=> 785054),
	array("label"=> "2020", "y"=> 874865),
	array("label"=> "2021", "y"=> 813264)
);

$range1519ttl = array(
	array("label"=> "2019", "y"=> 720094),
	array("label"=> "2020", "y"=> 825828),
	array("label"=> "2021", "y"=> 834848)
);

$range2024ttl = array(
	array("label"=> "2019", "y"=> 756189),
	array("label"=> "2020", "y"=> 818069),
	array("label"=> "2021", "y"=> 854382)
);

$range2529ttl = array(
	array("label"=> "2019", "y"=> 929123),
	array("label"=> "2020", "y"=> 835638),
	array("label"=> "2021", "y"=> 859215)
);

$range3034ttl = array(
	array("label"=> "2019", "y"=> 1023219),
	array("label"=> "2020", "y"=> 861109),
	array("label"=> "2021", "y"=> 883303)
);

$range3539ttl = array(
	array("label"=> "2019", "y"=> 983591),
	array("label"=> "2020", "y"=> 974357),
	array("label"=> "2021", "y"=> 873639)
);

$range4044ttl = array(
	array("label"=> "2019", "y"=> 861840),
	array("label"=> "2020", "y"=> 913386),
	array("label"=> "2021", "y"=> 856926)
);

$range4549ttl = array(
	array("label"=> "2019", "y"=> 730336),
	array("label"=> "2020", "y"=> 799344),
	array("label"=> "2021", "y"=> 787945)
);

$range5054ttl = array(
	array("label"=> "2019", "y"=> 610235),
	array("label"=> "2020", "y"=> 670571),
	array("label"=> "2021", "y"=> 680391)
);

$range5559ttl = array(
	array("label"=> "2019", "y"=> 485560),
	array("label"=> "2020", "y"=> 526403),
	array("label"=> "2021", "y"=> 560584)
);

$range6064ttl = array(
	array("label"=> "2019", "y"=> 359213),
	array("label"=> "2020", "y"=> 377636),
	array("label"=> "2021", "y"=> 422277)
);

$range65ttl = array(
	array("label"=> "2019", "y"=> 483619),
	array("label"=> "2020", "y"=> 530102),
	array("label"=> "2021", "y"=> 618115)
);


$jumlah_all = array( 
	array("label"=>"Jumlah Pria 2019", "symbol" => "P19","y"=>5285321),
	array("label"=>"Jumlah Pria 2020", "symbol" => "P20","y"=>5334781),
	array("label"=>"Jumlah Pria 2021", "symbol" => "P21","y"=>5362748),
	array("label"=>"Jumlah Wanita 2019", "symbol" => "W19","y"=>5272489),
	array("label"=>"Jumlah Wanita 2020", "symbol" => "W20","y"=>5227307),
	array("label"=>"Jumlah Wanita 2021", "symbol" => "W21","y"=>5282028),
	array("label"=>"Ttl Pria&Wanita 2019", "symbol" => "PW19","y"=>10557810),
	array("label"=>"Ttl Pria&Wanita 2020", "symbol" => "PW20","y"=>10562088),
	array("label"=>"Ttl Pria&Wanita 2021", "symbol" => "PW21","y"=>10644776),
)

?>
<script type="text/javascript">
window.onload = function () {

//*************************************************************** Grafik PRIA ***********************************************//
var chart = new CanvasJS.Chart("chartContainer", {
	title: {
		text: "Grapik Kelompok Pria dan Umur"
	},
	theme: "light2",
	animationEnabled: true,
	toolTip:{
		shared: true,
		reversed: true
	},
	axisY: {
		title: "Jumlah Penduduk Pria",
		suffix: ""
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeries
	},
	data: [
		{
			type: "stackedColumn",
			name: "AGE 0 - 4",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range04, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 5 - 9",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range59, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 10 - 14",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1014, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 15 - 19",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1519, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 20 - 24",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2024, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 25 - 29",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2529, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 30 - 34",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3034, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 35 - 39",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3539, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 40 - 44",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4044, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 45 - 49",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4549, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 50 - 54",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5054, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 55 - 59",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5559, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 60 - 64",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range6064, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 65+",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range65, JSON_NUMERIC_CHECK); ?>
		}
	]
});
 
chart.render();
 
function toggleDataSeries(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart.render();
}

//*************************************************************** Grafik WANITA ***********************************************//
var chart_wanita = new CanvasJS.Chart("chartContainerWanita", {
	title: {
		text: "Grapik Kelompok Wanita dan Umur"
	},
	theme: "light2",
	animationEnabled: true,
	toolTip:{
		shared: true,
		reversed: true
	},
	axisY: {
		title: "Jumlah Penduduk Wanita",
		suffix: ""
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeriesWanita
	},
	data: [
		{
			type: "stackedColumn",
			name: "AGE 0 - 4",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range04w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 5 - 9",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range59w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 10 - 14",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1014w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 15 - 19",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1519w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 20 - 24",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2024w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 25 - 29",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2529w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 30 - 34",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3034w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 35 - 39",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3539w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 40 - 44",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4044w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 45 - 49",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4549w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 50 - 54",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5054w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 55 - 59",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5559w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 60 - 64",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range6064w, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 65+",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range65w, JSON_NUMERIC_CHECK); ?>
		}
	]
});
 
chart_wanita.render();
 
function toggleDataSeriesWanita(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart_wanita.render();
}

//***************************************************** Grafik JUMLAH PRIA DAN WANITA ***********************************************//
var chart_ttl = new CanvasJS.Chart("chartContainerTtl", {
	title: {
		text: "Grapik Kelompok Jumlah Pria dan Wanita berdasarkan Umur"
	},
	theme: "light2",
	animationEnabled: true,
	toolTip:{
		shared: true,
		reversed: true
	},
	axisY: {
		title: "Jumlah Penduduk Total",
		suffix: ""
	},
	legend: {
		cursor: "pointer",
		itemclick: toggleDataSeriesTotal
	},
	data: [
		{
			type: "stackedColumn",
			name: "AGE 0 - 4",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range04ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 5 - 9",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range59ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 10 - 14",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1014ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 15 - 19",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range1519ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 20 - 24",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2024ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 25 - 29",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range2529ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 30 - 34",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3034ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 35 - 39",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range3539ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 40 - 44",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4044ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 45 - 49",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range4549ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 50 - 54",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5054ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 55 - 59",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range5559ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 60 - 64",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range6064ttl, JSON_NUMERIC_CHECK); ?>
		},{
			type: "stackedColumn",
			name: "AGE 65+",
			showInLegend: true,
			yValueFormatString: "#,##0",
			dataPoints: <?php echo json_encode($range65ttl, JSON_NUMERIC_CHECK); ?>
		}
	]
});
 
chart_ttl.render();
 
function toggleDataSeriesTotal(e) {
	if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
		e.dataSeries.visible = false;
	} else {
		e.dataSeries.visible = true;
	}
	e.chart_ttl.render();
}

var chart_jml = new CanvasJS.Chart("chartContainerJml", {
	theme: "light2",
	animationEnabled: true,
	title: {
		text: "Jumlah Pria dan Wanita 2019 - 2021"
	},
	data: [{
		type: "doughnut",
		indexLabel: "{symbol} - {y}",
		yValueFormatString: "#,##0\"\"",
		showInLegend: true,
		legendText: "{label} : {y}",
		dataPoints: <?php echo json_encode($jumlah_all, JSON_NUMERIC_CHECK); ?>
	}]
});
chart_jml.render();

 
}
</script>