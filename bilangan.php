<!DOCTYPE html>
<html lang="en">
    <head>
        <title>testing yudiana</title>
        <style>
            body {
                text-align: left;
            }
            h2, h4{
                margin: 0;
            }
            p#result_bilangan {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <h2>1. buatkan satu halaman untuk mengetahui bilangan ganjil / genap dari sebuah inputan</h2>
        <a href="/"><h1>Back</h1></a>
        <br >
        <!-- Penginputan Bilangan yang akan Dicek -->
        <p>input variable <input type="number" id="variable"> <button onclick="cekbilangan()">Cek</button></p>
        result: <b><label id="result_bilangan"></label></b>
        <script>

            function cekbilangan(){
                var bilangan = document.getElementById("variable").value
                if(bilangan % 2 == 0)
                    document.getElementById("result_bilangan").innerText = bilangan+" adalah Bilangan Genap"
                else
                    document.getElementById("result_bilangan").innerText = bilangan+" adalah Bilangan Ganjil"
            }
        </script>
    </body>
</html>